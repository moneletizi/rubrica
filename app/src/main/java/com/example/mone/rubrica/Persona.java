package com.example.mone.rubrica;

public class Persona {

    private String name;
    private String surname;
    private String CF;

    public Persona(String nomeP, String cognomeP, String CFP){
        this.name = nomeP;
        this.surname = cognomeP;
        this.CF = CFP;
    }

    public String visualizzadettagliPersona(){
        return "il nome è: "+ name + "\n"+
                "il cognome è: "+surname + "\n"+
                "il codice fiscale è: "+CF + "\n";
    }

}
