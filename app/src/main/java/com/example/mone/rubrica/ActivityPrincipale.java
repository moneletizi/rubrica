package com.example.mone.rubrica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityPrincipale extends AppCompatActivity {

    EditText editTextNome;
    EditText editTextCognome;
    EditText editTextCF;
    Button buttonVisualizzaPersona;

    public static final String keyName = "KEYNAME";
    public static final String keySurname = "KEYSURNAME";
    public static final String keyCF = "KEYCF";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);
        editTextNome = findViewById(R.id.edtNome);
        editTextCognome = findViewById(R.id.edtCognome);
        editTextCF = findViewById(R.id.edtCF);
        buttonVisualizzaPersona = findViewById(R.id.btnVisualizzaPersona);
        buttonVisualizzaPersona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nomeInserito = editTextNome.getText().toString();
                String cognomeInserito = editTextCognome.getText().toString();
                String CFinserito = editTextCF.getText().toString();
                if(nomeInserito.isEmpty() ||
                   cognomeInserito.isEmpty()  ||
                        CFinserito.isEmpty()){
                    Toast.makeText(getApplicationContext(),
                            "Riempire tutti i campi per favore",
                            Toast.LENGTH_LONG).show();
                }else{
                    //Persona persona1 = new Persona(nomeInserito, cognomeInserito, CFinserito);
                    //preparo i dati da inviare all'ACTIVITY
                    Intent intent = new Intent(ActivityPrincipale.this, PersonalDetailsActivity.class);
                    intent.putExtra(keyName, nomeInserito);
                    intent.putExtra(keySurname, cognomeInserito);
                    intent.putExtra(keyCF, CFinserito);
                    startActivity(intent);//Lancio l'activity passandogli i dati

                }

            }
        });

    }




}
