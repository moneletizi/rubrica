package com.example.mone.rubrica;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class PersonalDetailsActivity extends AppCompatActivity {

    TextView nameTxt;
    TextView surnameTxt;
    TextView cfTxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        nameTxt = findViewById(R.id.name_txt);
        surnameTxt = findViewById(R.id.surname_txt);
        cfTxt = findViewById(R.id.cftxt);

        //recuperiamo i dati passati dall'activity
        Intent intent = getIntent();
        if(intent!=null){
            //recupero i dati passati attraverso l'intent dell'activity che mi ha invocato
            String name = intent.getStringExtra(ActivityPrincipale.keyName);
            String surname = intent.getStringExtra(ActivityPrincipale.keySurname);
            String cf = intent.getStringExtra(ActivityPrincipale.keyCF);
            nameTxt.setText(name);
            surnameTxt.setText(surname);
            cfTxt.setText(cf);

        }

    }

}
